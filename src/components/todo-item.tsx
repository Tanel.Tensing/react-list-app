import * as React from 'react'

import { TodoItemInterface } from './../interfaces'

const TodoItem = (props: TodoItemInterface) => {
  return (
    <div>
      <div>
        <input
          value={props.todo.text}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => props.handleTodoUpdate(event, props.todo.id)}
        />
        <input
          type="button"
          value={props.todo.isCompleted ? 'Undo' : 'Set done'}
          onClick={() => props.handleTodoComplete(props.todo.id)}/>
      </div>
    </div>
  )
};

export default TodoItem
