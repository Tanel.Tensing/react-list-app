import * as React from 'react'
import { render } from 'react-dom'

import TodoForm from './components/todo-form'
import TodoList from './components/todo-list'

import { TodoInterface } from './interfaces'

const TodoListApp = () => {
  const [todos, setTodos] = React.useState<TodoInterface[]>([
    {id: '1', text: 'Wake up', isCompleted: false },
    {id: '2', text: 'Eat breakfast', isCompleted: false },
    {id: '3', text: 'Brush teeth', isCompleted: false }])

    function handleTodoCreate(todo: TodoInterface) {
        const newTodosState: TodoInterface[] = [...todos]
        newTodosState.push(todo)
        setTodos(newTodosState)
  }

    function handleTodoUpdate(event: React.ChangeEvent<HTMLInputElement>, id: string) {
        const newTodosState: TodoInterface[] = [...todos]
        newTodosState.find((todo: TodoInterface) => todo.id === id)!.text = event.target.value
        setTodos(newTodosState)
  }

    function handleTodoComplete(id: string) {
        const newTodosState: TodoInterface[] = [...todos]
        newTodosState.find((todo: TodoInterface) => todo.id === id)!.isCompleted = !newTodosState.find((todo: TodoInterface) => todo.id === id)!.isCompleted
        setTodos(newTodosState)
  }

  return (
    <div className="todo-list-app">
      <TodoForm
        todos={todos}
        handleTodoCreate={handleTodoCreate}
      />

      <TodoList
        todos={todos}
        handleTodoUpdate={handleTodoUpdate}
        handleTodoComplete={handleTodoComplete}
      />
    </div>
  )
}

const rootElement = document.getElementById('root')
render(<TodoListApp />, rootElement)
